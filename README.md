# Sample Web Apps
Docker Container for Demo WebApps

**Build Docker Image -**  
docker build -t sameerkm/employee-service:1.0.0 .

**Login to Docker Hub -**  
docker login

**Push Docker image to Repository -**  
docker push sameerkm/employee-service:1.0.0